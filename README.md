 
The common issue with dualboot windows + ubuntu:

"error: bad shim signature"

At (hd0,gpt4) linux /boot/vm...  error: bad shim signature.
   ... so you just need to enroll your hash of vmlinuz  linux kernel into the machine 
 then, boot a linux, that works and do:  mokutil --reset 
   enter pass for instance : user 
   [CODE]apt-get install pesign [/CODE]
Steps to Enroll the Kernel Linux: 
1. Enroll the hash of kernel image (the kernel image needs to decompressed first otherwise pesign can't recognize the hash) using 
mokutil:
  here we go :
   [CODE] mokutil --import-hash $( pesign -P -h -i vmlinuz-5.19.0-26-generic | cut -f 2 -d ' ')  [/CODE]

The command [CODE] dmesg | grep MOK[/CODE] shows the MOK variable.

2. Reboot and continue the hash enrollment in MOKManager 
3. Try to boot the kernel 

DMESG to check that your modules are all there. 
ip addr and wifi is there ;) 

--
[url]https://packages.ubuntu.com/jammy/amd64/pesign/filelist[/url]

f55229e9a22088ecfb751f359880cbd9  /boot/vmlinuz-4.9.0-11-amd64
e48935cc38ba4b74d1b80f91d61f7396  /boot/vmlinuz-5.19.0-21-generic
f7f09afba00c5b215f24e2a98e568f7b  /boot/vmlinuz-5.19.0-26-generic
eb6eb210c22a9fc6a9033322cc5050c6  /boot/vmlinuz-5.19.0-46-generic
81ec667774bc600e79958d8a26e6a1af  /boot/vmlinuz-6.2.0-26-generic
badb80ea6814bf13e255b3729e3890fd  /boot/vmlinuz-6.2.0-33-generic
83203d2c2621a0b75a979b1ac027fac1  /boot/vmlinuz-6.2.0-37-generic

